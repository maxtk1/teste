import React from 'react';
import styles from './App.module.css';
import CarOfferForm from './components/CarOfferForm';
import { Car } from './components/Icons/Car';
import { Motorcycle } from './components/Icons/Motorcycle';
import ShoppingRegion from './components/ShoppingRegion';
import { CarOfferProvider } from './context/CarOffer';

const App = () => {
  return (
    <CarOfferProvider>
      <section className={styles.content}>
        <img src="/webmotors.png" alt="webmotors" width={200} height={38} />
        <div className={styles.regionGroup}>
          <ShoppingRegion active icon={<Car />} title="carros" purchaseLabel="comprar" key={2} />
          <ShoppingRegion icon={<Motorcycle />} title="motos" purchaseLabel="comprar" key={2} />
        </div>
        <CarOfferForm />
      </section>
    </CarOfferProvider>
  );
}

export default App;
