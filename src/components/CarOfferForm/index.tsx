import { FC, FormEvent, FormEventHandler, useRef } from "react";
import useCarOffer from "../../hooks/useCarOffer";
import { Make } from "../../types/Make";
import { Model } from "../../types/Model";
import { Version } from "../../types/Version";
import Select from "../Select";
import styles from './CarOfferForm.module.css';

const prices = [25000, 35000, 45000, 60000, 70000];
const years = [2010, 2012, 2014, 2018, 2020];

const CarOfferForm: FC = () => {
  const newRef = useRef<HTMLInputElement>({} as HTMLInputElement);
  const usedRef = useRef<HTMLInputElement>({} as HTMLInputElement);

  const radius = [100, 200];

  const radiusOptionList = (radius: number) => (
    <option className={styles.bold} value={radius} key={radius}>{radius}km</option>
  );

  const { makes, make, setMake, models, model, setModel, version, versions, setVersion } = useCarOffer();

  const makeOptionList = (make: Make) => (
    <option value={make.ID} key={make.ID}>{make.Name}</option>
  );

  const modelOptionList = (model: Model) => (
    <option value={model.ID} key={model.ID}>{model.Name}</option>
  );

  const versionOptionList = (version: Version) => (
    <option value={version.ID} key={version.ID}>{version.Name}</option>
  );

  const priceOptionList = (price: number) => (
    <option key={price} value={price}>
      {price.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL', minimumFractionDigits: 0 })}
      </option>
  );

  const yearOptionList = (year: number) => (
    <option key={year} value={year}>{year}</option>
  );

  return (
    <>
      <form className={styles.content}>
        <div className={styles.sectionContent}>
        <div className={styles.sectionCheck}>
          <div>
            <input ref={newRef} type="checkbox" />
            <label>Novos</label>
          </div>
          <div>
            <input ref={usedRef} type="checkbox" />
            <label>Usados</label>
          </div>
        </div>
          <div className={styles.firstSection}>
            <div className={styles.location}>
              <Select label="Onde: ">
                <option>São Paulo - SP</option>
              </Select>
              <Select label="Raio: ">
                {radius.map(radiusOptionList)}
              </Select>
            </div>
            <Select value={make} onChange={(event) => setMake(event.target.value)} label="Marca: ">
              {makes.data && makes?.data?.map(makeOptionList)}
            </Select>
            <Select value={model} onChange={(event) => setModel(event.target.value)} label="Modelo: ">
              {make && models?.data?.map(modelOptionList)}
            </Select>
          </div>
          <div className={styles.secondSession}>
            <Select label="Ano Desejado: ">
              {years.map(yearOptionList)}
            </Select>
            <Select label="Faixa de Preço: ">
              {prices.map(priceOptionList)}
            </Select>
            <Select value={version} onChange={(event) => setVersion(event.target.value)} label="Versão: ">
              {model && versions.data?.map(versionOptionList)}
            </Select>
          </div>
          <div className={styles.actionGroup}>
            <button>Busca Avançada</button>
            <div>
              <button>Limpar Filtros</button>
              <button type="submit">Ver Ofertas</button>
            </div>
          </div>
        </div>
      </form>
    </>
  )
}

export default CarOfferForm;
