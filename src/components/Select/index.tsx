import React, { FC } from "react";
import styles from './Select.module.css'

export interface SelectProps {
  label: string;
}

const Select: FC<SelectProps & React.DetailedHTMLProps<React.SelectHTMLAttributes<HTMLSelectElement>, HTMLSelectElement>> = (props) => {
  return (
    <>
      <div className={styles.selectContent}>
        <span className={styles.label}>{props.label}</span>
        <select {...props} className={styles.select}>
          {props.children}
        </select>
      </div>
    </>
  )
}

export default Select;
