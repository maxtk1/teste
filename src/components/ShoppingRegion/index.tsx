import { FC, ReactNode } from "react";
import styles from './ShoppingRegion.module.css';

export interface ShoppingRegionProps {
  active?: boolean;
  purchaseLabel: string;
  title: string;
  icon: ReactNode;
}

const ShoppingRegion: FC<ShoppingRegionProps> = ({ active = false, purchaseLabel, title, icon }) => {
  return (
    <>
      <div className={[active ? styles.activeRegion : styles.region, styles.content].join(' ')}>
        <div>
        {icon}
        </div>
        <div>
          <span>{purchaseLabel}</span>
          <p>{title}</p>
        </div>
      </div>
    </>
  )
}

export default ShoppingRegion;
