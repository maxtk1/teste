/* eslint-disable react-hooks/rules-of-hooks */
import { createContext, FC, useEffect, useState, Dispatch, SetStateAction } from 'react';
import useSWR, { SWRResponse } from 'swr';
import { Make } from '../types/Make';
import { Model } from '../types/Model';
import { Vehicle } from '../types/Vehicle';
import { Version } from '../types/Version';

interface CarOfferContextProps {
  makes: SWRResponse<Make[], any>;
  models: SWRResponse<Model[], any>;
  versions: SWRResponse<Version[], any>;
  make: string;
  model: string;
  version: string;
  setMake: Dispatch<React.SetStateAction<string>>;
  setModel: Dispatch<React.SetStateAction<string>>;
  setVersion: Dispatch<React.SetStateAction<string>>;
}

const api = 'https://desafioonline.webmotors.com.br/api/OnlineChallenge';
const fetcher = (endpoint: string) => fetch(`${api}${endpoint}`).then((res) => res.json());
const getParams = (params: {}) => new URLSearchParams(params).toString();

export const CarOfferContext = createContext({} as CarOfferContextProps);


export const CarOfferProvider: FC = ({ children }) => {
  const [make, setMake] = useState("1");
  const [model, setModel] = useState("1");
  const [version, setVersion] = useState("1"); 

  const makes = useSWR<Make[]>('/Make', fetcher);
  const models = useSWR<Model[]>(`/Model?${getParams({ MakeID: make })}`, fetcher);
  const versions = useSWR<Version[]>(`/Version?${getParams({ ModelID: model })}`, fetcher);

  useEffect(() => {
    return () => {
      setMake("");
      setModel("");
      setVersion("");
    }
  }, []);

  return (
    <CarOfferContext.Provider value={{ makes, models, versions, make, model, version, setMake, setModel, setVersion }}>
      {children}
    </CarOfferContext.Provider>
  )
} 
