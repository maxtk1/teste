import { useContext } from "react"
import { CarOfferContext } from "../context/CarOffer"

const useCarOffer = () => {
  const carOffer = useContext(CarOfferContext);

  return carOffer;
}

export default useCarOffer;
