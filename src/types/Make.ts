export type Make = {
  ID: number;
  Name: string;
}