export type Model = {
  ID: number;
  MakeID: number;
  Name: string;
}