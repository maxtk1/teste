export type Vehicle = {
  ID: number;
  Make: string;
  Model: string;
  Version: string;
  Image: string;
  KM: number,
  Price: number;
  YearModel: number;
  YearFab: number;
  Color: string;
}