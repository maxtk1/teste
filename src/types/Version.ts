export type Version = {
  ID: number;
  ModelID: number;
  Name: string;
}